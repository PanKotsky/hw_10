const connection = require('./connection');

const options = process.argv.reduce((acc, cur) => {
    if (cur.includes('--')) {
        const paramValue = cur.split('=');
        acc[paramValue[0].replace('--', '')] = paramValue[1];
    }
    return acc;
}, {});

function exec(data) {
    const limit = data.size;
    const offset = data.page > 1 ? (data.page-1) * limit : 0;
    let result = connection.query(
        `SELECT *
         FROM videos
         ORDER BY id
         LIMIT $1
         OFFSET $2;`, [limit, offset]);
    connection.end();
    return result;
}

exec(options)
    .then(result => console.log('Selected next row: ', result.rows))
    .catch(err => console.error(err));

module.exports = {
    exec: exec
}