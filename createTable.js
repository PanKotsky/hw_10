const connection = require('./connection');

connection.query(
    `CREATE TABLE IF NOT EXISTS videos
     (
         id serial primary key,
         title text,
         views float4,
         category text
     );`,
    (error, results) => {
        if (error) {
            throw error;
        }
        console.log(results);
    }
);
connection.end();