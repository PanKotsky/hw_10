const connection = require('./connection');

const options = process.argv.reduce((acc, cur) => {
    if (cur.includes('--')) {
        const paramValue = cur.split('=');
        acc[paramValue[0].replace('--', '')] = paramValue[1];
    }
    return acc;
}, {});

function exec(data) {
    const pattern  = data.search;
    let query = `SELECT * FROM videos`;
    query += (pattern) ? ` WHERE title LIKE '%${pattern}%'` : '';
    let result = connection.query(
        query
    );
    connection.end();
    return result;
}

exec(options)
    .then(result => console.log('Selected next record: ', result.rows))
    .catch(err => console.error(err));

module.exports = {
    exec: exec
}