const connection = require('./connection');

const options = process.argv.reduce((acc, cur) => {
    if (cur.includes('--')) {
        const paramValue = cur.split('=');
        acc[paramValue[0].replace('--', '')] = paramValue[1];
    }
    return acc;
}, {});

function exec(data) {
    const limit = data.top;
    let result = connection.query(
        `SELECT category, SUM(views) as sumViews
         FROM videos
         GROUP BY category
         ORDER BY sumViews DESC
         LIMIT $1;`,
        [limit]
    );
    connection.end();
    return result;
}

exec(options)
    .then(result => console.log('Selected next rows: ', result.rows))
    .catch(err => console.error(err));

module.exports = {
    exec: exec
}