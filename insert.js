const connection = require('./connection');

const options = process.argv.reduce((acc, cur) => {
    if (cur.includes('--')) {
        const paramValue = cur.split('=');
        acc[paramValue[0].replace('--', '')] = paramValue[1];
    }
    return acc;
}, {});

function exec(data) {
    let result = connection.query(
        `INSERT INTO videos (title, views, category) VALUES ($1, $2, $3) RETURNING id`, [data.title, data.views, data.category]);
    connection.end();
    return result;
}

exec(options)
    .then(result => console.log('Inserted record with id: ', result.rows[0].id))
    .catch(err => console.error(err));

module.exports = {
    exec: exec
}
