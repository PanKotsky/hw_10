const connection = require('./connection');

const options = process.argv.reduce((acc, cur) => {
    if (cur.includes('--')) {
        const paramValue = cur.split('=');
        acc[paramValue[0].replace('--', '')] = paramValue[1];
    }
    return acc;
}, {});

function exec(data) {
    let query = `SELECT category, SUM(views) as sumViews FROM videos GROUP BY category`;
    let result = connection.query(
        query
    );
    connection.end();
    return result;
}

exec(options)
    .then(function(result) {
        for (let res of result.rows) {
            console.log('{' + res.category + '} - {' + res.sumviews + '}');
        }
    })
    .catch(err => console.error(err));

module.exports = {
    exec: exec
}